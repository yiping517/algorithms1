package selfpractive;

import java.util.ArrayList;
import java.util.List;


//Successione di Fibonacci 【費波那契數】- 使用 List
public class Fibonacci_List {

		public static void main(String[] args) {
			int[] arr = new int[] {0, 1};
			int result = 0;
			List<Integer> resultList = new ArrayList<>();
			
			for (int i=0; i<10; i++) {
				if (i<2) {
					result = arr[i];
				} else {
					result = resultList.get(i-2) + resultList.get(i-1);
				}
				resultList.add(result);
				
				System.out.print(result + ",");
			}
		}
}
