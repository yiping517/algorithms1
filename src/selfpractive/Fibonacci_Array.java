package selfpractive;



//Successione di Fibonacci 【費波那契數】- 使用 Array => 不可以。因為需要向前移動。
public class Fibonacci_Array { 

		public static void main(String[] args) {
			int[] arr = new int[] {0, 1};
			int result = 0;
//			List<Integer> resultList = new ArrayList<>();
			int[] resultArr = new int[3]; 
			
			for (int i=0; i<10; i++) {
				if (i<2) {
					result = arr[i];
				} else {
//					result = resultList.get(i-2) + resultList.get(i-1);
					result = resultArr[(i-2)%3] + resultArr[(i-1)%3];
				}
//				resultList.add(result);
				
				if (i % 3 == 1) {
					resultArr[0] = result;
				} else if (i % 3 == 2) {
					resultArr[1] = result;
				} else {
					resultArr[2] = result;
				}
				
				System.out.print(result + ",");
			}
		}
}
